package main

import (
	"log"
	"net/http"
	"strings"

	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/feature/login"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/feature/register"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/middleware"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/repository"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/storage/database"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

func init() {
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
}

func main() {
	router := mux.NewRouter()
	router.Use(middleware.Auth)

	//storage
	database := database.New()
	user := repository.NewUser(database)

	// handler routes
	register.NewRoutes(router, user)
	login.NewRoutes(router, user)

	err := http.ListenAndServe(":8000", router)
	if err != nil {
		log.Fatal("Serve error:", err)
	}
}
