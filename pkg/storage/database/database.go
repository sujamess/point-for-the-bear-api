package database

import (
	"time"

	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/config"
	"github.com/globalsign/mgo"
)

// IDatabase is a wrapper of database struct
type IDatabase interface {
	Find(collection string, query, result interface{}) error
	Insert(collection string, docs interface{}) error
}

// Database is a struct of database
type Database struct {
	connection *mgo.Database
}

// New is an instantiate of database
func New() *Database {
	session, err := mgo.DialWithTimeout(config.MongoURL, time.Second*30)
	if err != nil {
		panic(err)
	}

	return &Database{
		connection: session.Copy().DB(config.MongoDatabase),
	}
}

// Find record from database
func (db *Database) Find(collection string, query, result interface{}) error {
	return db.connection.C(collection).Find(query).One(result)
}

// Insert new record to database
func (db *Database) Insert(collection string, docs interface{}) error {
	return db.connection.C(collection).Insert(docs)
}
