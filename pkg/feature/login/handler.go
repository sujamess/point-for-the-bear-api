package login

import (
	"net/http"

	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/request"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/response"
)

// Handler is a struct of login handler
type Handler struct {
	svc IService
}

// NewHandler is an instantiate of login handler
func NewHandler(service IService) *Handler {
	return &Handler{
		svc: service,
	}
}

// Login for authorization user
func (hdlr *Handler) Login(w http.ResponseWriter, r *http.Request) {
	req := request.New(r)

	auth := &Auth{}
	err := req.GetBodyAndConvertToStruct(auth)
	if err != nil {
		response.Error(w, http.StatusBadRequest, err)
		return
	}

	apiErr := hdlr.svc.ValidateLogin(auth)
	if apiErr != nil {
		response.APIError(w, apiErr)
		return
	}

	response.Success(w, "Login successfully")
	return
}
