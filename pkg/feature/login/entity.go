package login

// Auth is a struct for user's authorization
type Auth struct {
	Phone    string `bson:"phone" json:"phone"`
	Password string `bson:"password" json:"password"`
}
