package login

import (
	"net/http"

	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/repository"
	"github.com/gorilla/mux"
)

// NewRoutes is an instantiate of login routes
func NewRoutes(router *mux.Router, user repository.IUser) {
	service := NewService(user)
	handler := NewHandler(service)

	login := router.PathPrefix("/login").Subrouter()

	login.Methods(http.MethodPost).Path("").HandlerFunc(handler.Login)
}
