package login

import "errors"

var (
	errUserNotFound = errors.New("User not found")
)
