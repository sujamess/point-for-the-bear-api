package login

import (
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/api"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/feature/register"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/repository"
)

// IService is a wrapper of login service struct
type IService interface {
	ValidateLogin(auth *Auth) *api.Error
}

// Service is a struct of login service
type Service struct {
	user repository.IUser
}

// NewService is an instantiate of login service
func NewService(user repository.IUser) *Service {
	return &Service{
		user: user,
	}
}

// ValidateLogin is for authorization user
func (svc *Service) ValidateLogin(auth *Auth) *api.Error {
	user := &register.UserAuth{}

	err := svc.user.Authorization(auth, user)
	if err != nil {
		apiErr := api.NewError(errUserNotFound)
		return apiErr
	}

	return nil
}
