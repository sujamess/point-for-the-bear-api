package register

import (
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/api"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/cipher"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/repository"
)

// IService is a wrapper of register service struct
type IService interface {
	NewUser(data *UserAuth) *api.Error
}

// Service is a struct of register service
type Service struct {
	user repository.IUser
}

// NewService is an instantiate of register service
func NewService(user repository.IUser) *Service {
	return &Service{
		user: user,
	}
}

// NewUser inserting new user to storage
func (svc *Service) NewUser(newUser *UserAuth) *api.Error {
	existedUser := &UserAuth{}
	err := svc.user.FindByPhone(newUser.Phone, existedUser)
	if existedUser != (&UserAuth{}) {
		apiErr := api.NewError(errUserExist)
		return apiErr
	} else if err != nil {
		apiErr := api.NewError(err)
		return apiErr
	}

	newUser.Password = cipher.Hash(newUser.Password)

	err = svc.user.Insert(newUser)
	if err != nil {
		apiErr := api.NewError(err)
		return apiErr
	}

	return nil
}
