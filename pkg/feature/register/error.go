package register

import "errors"

var (
	errUserExist = errors.New("User already exist")
)
