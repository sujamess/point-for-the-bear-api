package register

// UserAuth is a struct of user's auth
type UserAuth struct {
	Phone     string `bson:"phone" json:"phone"`
	Password  string `bson:"password" json:"password"`
	Firstname string `bson:"firstname" json:"firstname"`
	Lastname  string `bson:"lastname" json:"lastname"`
	Nickname  string `bson:"nickname" json:"nickname"`
	Address   string `bson:"address" json:"address"`
}
