package register

import (
	"net/http"

	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/request"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/response"
)

// Handler is a struct of register handler
type Handler struct {
	svc IService
}

// NewHandler is an instantiate of register handler
func NewHandler(service IService) *Handler {
	return &Handler{
		svc: service,
	}
}

// NewUser creating new user
func (hdlr *Handler) NewUser(w http.ResponseWriter, r *http.Request) {
	req := request.New(r)

	userAuth := &UserAuth{}
	err := req.GetBodyAndConvertToStruct(userAuth)
	if err != nil {
		response.Error(w, http.StatusBadRequest, err)
		return
	}

	apiErr := hdlr.svc.NewUser(userAuth)
	if err != nil {
		response.APIError(w, apiErr)
		return
	}

	response.Success(w, "Register successfully")
	return
}
