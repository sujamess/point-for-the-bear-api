package register

import (
	"fmt"
	"net/http"

	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/repository"
	"github.com/gorilla/mux"
)

// NewRoutes is an instantiate of register routes
func NewRoutes(router *mux.Router, user repository.IUser) {
	service := NewService(user)
	handler := NewHandler(service)

	router.Methods(http.MethodGet).Path("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, World")
	})

	// restful path
	register := router.PathPrefix("/register").Subrouter()
	register.Methods(http.MethodPost).Path("").HandlerFunc(handler.NewUser)
}
