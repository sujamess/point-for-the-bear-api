package middleware

import (
	"errors"
	"net/http"

	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/config"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/request"
	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/response"
)

const token = "Authorization"

var errUnauthorized = errors.New("Unauthorized access")

// Auth middleware
func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := request.New(r)
		authorization := req.GetHeader(token)

		if authorization != config.Authorization {
			response.Error(w, http.StatusUnauthorized, errUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}
