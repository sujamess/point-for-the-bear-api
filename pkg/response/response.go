package response

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/sujamess/point-for-the-bear-api/pkg/api"
)

// List of http status code
const (
	Ok int = http.StatusOK
)

// Response is a struct of response
type Response struct {
	Data    interface{} `json:"data,omitempty"`
	Message string      `json:"message,omitempty"`
}

// Success write success response to client
func Success(w http.ResponseWriter, payload interface{}) {
	response := &Response{
		Data: payload,
	}

	write(w, Ok, response)
}

// Error write error resposne to client
func Error(w http.ResponseWriter, httpStatusCode int, err error) {
	response := &Response{
		Message: err.Error(),
	}

	write(w, httpStatusCode, response)
}

// APIError write custom error (API Error) response to client
func APIError(w http.ResponseWriter, err *api.Error) {
	response := &Response{
		Message: err.Error(),
	}

	write(w, err.HTTPStatusCode, response)
}

// write http response
func write(w http.ResponseWriter, httpStatusCode int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(int(httpStatusCode))
	json.NewEncoder(w).Encode(payload)
}
