package api

import (
	"errors"
	"net/http"
)

// List of available errors
var (
	errUserExist           = errors.New("User already exist")
	errUserNotFound        = errors.New("User not found")
	errInternalServerError = errors.New("Internal server error")
)

// Error is a struct of error
type Error struct {
	HTTPStatusCode int
	Message        string
}

// Error returns error message
func (err *Error) Error() string {
	return err.Message
}

// NewError is an instantiate of error
func NewError(err error) *Error {
	return &Error{
		HTTPStatusCode: getHTTPStatusCode(err),
		Message:        err.Error(),
	}
}

func getHTTPStatusCode(err error) (httpStatusCode int) {
	switch err {
	case errUserExist:
		httpStatusCode = http.StatusBadRequest
	case errUserNotFound:
		httpStatusCode = http.StatusNotFound
	default:
		httpStatusCode = http.StatusInternalServerError
	}

	return httpStatusCode
}
