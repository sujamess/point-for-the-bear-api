package request

import (
	"encoding/json"
	"net/http"
	"net/url"
)

// Request is a struct for request
type Request struct {
	req  *http.Request
	form url.Values
}

// New is an instantiate of request
func New(r *http.Request) *Request {
	return &Request{
		req:  r,
		form: r.Form,
	}
}

// GetBodyAndConvertToStruct is getting body from request then covnvert to struct
func (r *Request) GetBodyAndConvertToStruct(desc interface{}) error {
	body := r.req.Body

	return json.NewDecoder(body).Decode(desc)
}

// GetForm is getting form values from request
func (r *Request) GetForm(key string) string {
	return r.form.Get(key)
}

// GetQueryString is getting query string from request
func (r *Request) GetQueryString(key string) string {
	return r.req.URL.Query().Get(key)
}

// GetHeader is getting header value from request
func (r *Request) GetHeader(key string) string {
	return r.req.Header.Get(key)
}
