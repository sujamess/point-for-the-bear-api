package repository

import "bitbucket.org/sujamess/point-for-the-bear-api/pkg/storage/database"

// IUser is an wrapper of user repository struct
type IUser interface {
	FindByPhone(phone string, result interface{}) error
	Authorization(query, result interface{}) error
	Insert(data interface{}) error
}

// User is a struct of user repository
type User struct {
	db database.IDatabase
}

// NewUser is an instantiate of user repository
func NewUser(database database.IDatabase) *User {
	return &User{
		db: database,
	}
}

// FindByPhone returns user data who has a given phone
func (repo *User) FindByPhone(phone string, result interface{}) error {
	query := make(map[string]interface{})
	query["phone"] = phone

	return repo.db.Find(CollectionUser, query, result)
}

// Authorization returns user data who has successfully authorization
func (repo *User) Authorization(query, result interface{}) error {
	return repo.db.Find(CollectionUser, query, result)
}

// Insert new data to storage
func (repo *User) Insert(data interface{}) error {
	return repo.db.Insert(CollectionUser, data)
}
