package config

import "github.com/spf13/viper"

// Database config
var (
	MongoDatabase = viper.GetString("MONGO.DATABASE")
	MongoURL      = viper.GetString("MONGO.URL")
)

// Const config
var (
	Authorization = viper.GetString("PFTB.Authorization")
)
